<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class News extends Model
{
    use HasFactory;

    public function publisher()
    {
        return $this->belongsTo(Publisher::class);
    }

    public function mapper(): array
    {
        return  [
            'id' => $this->id,
            'header' => $this->header,
            'seo_tags' => $this->seo_tags,
            'slug' => $this->slug,
            'meta' => $this->meta,
            'body' => $this->body,
            'publisher' => [
                'id' => $this->publisher_id ?? '',
                'name' => $this->publisher->name ?? 'user not exist',
            ],
        ];
    }
}
