<?php

declare(strict_types=1);

namespace App\Http\Controllers;

use App\Http\Requests\NewsRequest;
use App\Models\News;
use Symfony\Component\HttpFoundation\Response as ResponseAlias;

class NewsController extends Controller
{
    public function create(NewsRequest $request)
    {
        $news = new News();

        $news->header = $request['header'];
        $news->seo_tags = $request['seo_tags'];
        $news->slug = $request['slug'];
        $news->meta = $request['meta'];
        $news->body = $request['body'];
        $news->publisher_id = $request['publisher_id'];

        $news->save();

        return response()->json($news->mapper(), ResponseAlias::HTTP_CREATED);
    }

    public function showById(int $id)
    {
        $news = News::where('id', $id)->first();

        if (is_null($news)) {
            abort(404);
        }

        return response()->json([
            $news->mapper(),
        ]);
    }

    public function delete(int $id)
    {
        $news = News::query()->where('id','=',$id)->first();

        if (is_null($news)) {
            abort(404);
        }

        $news->delete();

        return response()->json(null, ResponseAlias::HTTP_NO_CONTENT);
    }

    public function showAll()
    {
        $newsList = [];

        $news = News::paginate(15);

        foreach ($news as $item) {
            $newsList[] = $item->mapper();
        }

        return response()->json([
            'news' => $newsList,
        ]);
    }
}
