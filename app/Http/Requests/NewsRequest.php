<?php

declare(strict_types=1);

namespace App\Http\Requests;

use Illuminate\Contracts\Validation\Validator;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Exceptions\HttpResponseException;

class NewsRequest extends FormRequest
{
    public function rules()
    {
        return [
            'header' => 'required|max:50',
            'seo_tags' => 'required|max:255',
            'slug' => 'required|max:50',
            'meta' => 'required|max:255',
            'body' => 'required|max:255',
            'publisher_id' => 'required|integer',
        ];
    }

    protected function failedValidation(Validator $validator)
    {
        throw new HttpResponseException(
            response()->json($validator->errors(), 422)
        );
    }
}
