<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::post('/news', [\App\Http\Controllers\NewsController::class, 'create']);
Route::get('/news', [\App\Http\Controllers\NewsController::class, 'showAll']);
Route::delete('/news/{id}', [\App\Http\Controllers\NewsController::class, 'delete'])
    ->where('id', '[0-9]+');
Route::get('/news/{id}', [\App\Http\Controllers\NewsController::class, 'showById'])
    ->where('id', '[0-9]+');

Route::get('/', function () {
    return view('welcome');
});
